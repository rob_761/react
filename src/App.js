import React, { useState } from 'react';
import classes from './App.module.css';
import Person from './Person/Person'

const App  = (props) => {
    const [personsState, setPersonsState] = useState({
        persons: [
            {id: 1, name: 'Rob', age:28},
            {id: 2, name: 'Jim', age:33},
            {id: 3, name: 'Kate', age:35}
        ],
        showPersons: false
    });

   const deletePersonHandler = (personIndex) => {
        const persons = [...personsState.persons]
        persons.splice(personIndex, 1);
        setPersonsState({
            ...personsState,
            persons: persons
        });
   }

    const nameChangedHandler = (event, id) => {
        const personIndex = personsState.persons.findIndex(person => {
            return person.id === id
        });

        const person = {...personsState[personIndex]};
        person.name = event.target.value;

        const persons = [...personsState.persons];
        persons[personIndex] = person;

        setPersonsState({
            ...personsState,
            persons: persons
        });
    }

    const  togglePersonHandler = () =>  {
        const doesShow = personsState.showPersons;
        setPersonsState({
            ...personsState,
            showPersons: !doesShow});
    }

    const getPersons = () => {
        let persons = null;

        if (personsState.showPersons) {
            persons = (
                <div>
                    {
                        personsState.persons.map((person, index) => {
                            return <Person
                                click={() => deletePersonHandler(index)}
                                name={person.name}
                                age={person.age}
                                key={person.id}
                                changed={(event) => nameChangedHandler(event, person.id)}/>
                        })
                    }
                </div>
            );
        }
      return persons;
    }

    const getClasses = () => {
        const assignedClasses = [];
        if (personsState.persons.length <= 2) {
            assignedClasses.push(classes.red);
        }
        if (personsState.persons.length <= 1) {
            assignedClasses.push(classes.bold);
        }
        return assignedClasses.join(' ');
   }

    const getBtnClasses = () => {
        return personsState.showPersons? classes.Red : '';
    }




    return (
      <div className={classes.App}>
        <h1>Hi, I'm a React App</h1>
          <p className={getClasses()}>This is working</p>
          <button
              className={getBtnClasses()}
              onClick={togglePersonHandler}>Toggle Persons</button>
          {getPersons()}
      </div>
    );
}


export default App;
